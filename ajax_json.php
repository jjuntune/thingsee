<?php
include_once 'class/thingsee_rest.php';

header('Content-type: application/json');        
$json_str='[';

$thingsee_rest=new Thingsee_Rest("http://hackoulu2015.thingsee.com/v1", "jouni.juntunen@oamk.fi","Thingsee2015!");                
$json= $thingsee_rest->get("events?type=sense");        
foreach ($json["events"] as $event) {
	$engine=$event["cause"];
	$senses=$engine["senses"];
	$val1=$senses[0];
	$val2=$senses[1];
	$latitude=$val1["val"];
	$longitude=$val2["val"];
	
	$json_str.="{";
	$json_str.='"latitude":"' .  $latitude . '",';
	$json_str.='"longitude":"' . $longitude . '"';            
	$json_str.="},"; 
}

$json_str=substr($json_str,0, strlen($json_str)-1);
$json_str.=']';
print $json_str;        