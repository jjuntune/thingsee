<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Thingsee_Rest
 *
 * @author jjuntune
 */
class Thingsee_Rest {
    private $server="";
    private $email="";
    private $password="";
    private $account_auth_token="";
    
    public function __construct($server,$email,$password) {
        $this->server=$server;        
        $this->account_auth_token=$this->authorize($email,$password);       
        $this->email=$email;
        $this->password=$password;        
    }
    
    public function get($service) {                        
        $ch = curl_init($this->server . "/" . $service);         
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");                                                                             
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        $auth="Authorization: Bearer " .  $this->account_auth_token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',
            $auth)                
        );                                                                                                                                                                                              
                
        $result = curl_exec($ch);          
        return json_decode($result,true);        
    }
    
    public function post($service,$param_string) {
        $ch = curl_init($this->server . "/" . $service);        
        $data_string = json_encode($param_string);                                                                                   
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");    
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
                
        $auth="Authorization: Bearer " .  $this->account_auth_token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',
            $auth)
            
        );                                                                                                                                                                                              

        $result = curl_exec($ch);        
        return json_decode($result,true);        
    }
    
    private function authorize($email,$password) {
        $ch = curl_init($this->server . "/accounts/login");     
        $data = array("email" => $email, "password" => $password);                        
        $data_string = json_encode($data);                                                                                   
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");    
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json')
        );                                                                                                                                                                                              

        $result = curl_exec($ch);        
        $json=json_decode($result,true);                        
        
        return $json['accountAuthToken'];
    }
}