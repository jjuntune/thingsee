<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
            <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
            <script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=true"></script>
            <script type="text/javascript" src="js/gmaps.js"></script>
            <style type="text/css">
                #map {width: 600px;height: 600px;}
            </style>
    </head>
    <body>
	<p>Thingsee spots</p>
	   <?php  
		$url="http://localhost/wp_thingsee";
        ?>
	    <script>
        var map;       
        $(document).ready(function(){     
				
				map = new GMaps({
					div: '#map',    
					zoom: 20,
					lat: 65.06049,
					lng: 25.27997
				});
				   
				function updateActivities()
				{							
					$.ajax({
						type: "POST",
						url: "ajax_json.php", 						
						success: function (output) {             						
							$.each(output,function(i,v) {
								map.setCenter(v.latitude,v.longitude);
								map.addMarker({
									lat: v.latitude,
									lng: v.longitude									
								});
							});						
						}   
					});	
				}
				setInterval(updateActivities,10000); 
		});    
		</script>

   
        <div id="map"></div>
        
    </body>
</html>
